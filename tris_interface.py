# encoding: utf-8
#
#  tris.tris_interface
import readline                     #@UnusedImport
from tris_module import Tri

class Ui(object):
    '''
    Interface console
    '''
    def __init__(self, **params):
        '''
        Constructeur
        @param params:
        '''
        self.objet_tri = {                          # objets tri
            'liste_1': None,                        # liste entiers
            'liste_2': None                         # liste entiers aléatoire
        }
        self.gabarit   = params['gabarit']          # masque menu
        self.dimension = params['dimension']        # dimension liste
        self.detail_msg= params['detail_msg']       # entête detail liste
        self.params = params                        # autres paramètres

        self.liste = 'liste_2'                      # liste courante
        self.ecrire_param('liste', self.liste)      # afficher liste courante

    def tri(self, liste=None):
        '''
        Retourne l'objet tri pointée par liste ou par liste courante
        @return: object Tri
        '''
        if liste:
            return self.objet_tri[liste]
        return self.objet_tri[self.liste]

    def installer_tri(self, liste, objet):
        '''
        Récuperer l'objet créé dans le tableau objet_tri
        '''
        self.objet_tri[liste] = objet

    def ecran_net(self, n=30):
        '''
        Effacer écran. (30 lignes vierges)
        @param n: int nb lignes vierges
        '''
        print( '\n'*n)

    def saisir_entier(self, message):
        '''
        Retourne un entier après saisie ou None si fin
        '''
        rep = ''
        while rep.upper()!='Q':
            rep = input('%s ou %s'% ('(Q)uitter', message) )
            try:
                return int(rep)
            except:
                pass
        return None

    def saisir_liste_entiers(self):
        '''
        Retourne une liste d'entiers après saisie
        @return: int list liste entiers
        '''
        rep = 0
        L = []
        while rep!=None:
            self.ecran_net()
            print("Tableau d'entiers:", L)
            rep = self.saisir_entier("Entier suivant: ")
            if type(rep) is int:
                L.append(rep)
        return L

    def afficher_message(self, message):
        '''
        Afficher un message en bas de page
        '''
        while True:
            self.ecran_net()
            print( self.detail_msg.format(self.liste, message))
            rep = input('Touche Entrée pour continuer: ')
            if not rep:
                break

    def change_liste(self):
        '''
        Bascule modifier la liste courante
        '''
        if self.liste == 'liste_1':
            self.liste = 'liste_2'
        else:
            self.liste = 'liste_1'
        return self.liste

    def lire_param(self, clef):
        return self.params.get(clef)

    def ecrire_param(self, clef, valeur):
        self.params[clef] = valeur

    def effacer_params(self, prefix):
        '''
        Raz paramètres dont le prefix clef commence par 'prefix'
        '''
        for k in self.params.keys():
            if k.startswith(prefix):
                self.params[k]=''

    def menu(self):
        self.ecran_net()
        print( self.gabarit.format(**self.params))

    def loop(self):
        '''
        Boucle principale
        '''
        while True:
            self.menu()
            c = input('Choix: ').upper()

            if c=="Q":
                break

            if c=='S':      # Saisie manuelle
                self.installer_tri( 'liste_1', Tri(self.saisir_liste_entiers() ) )  # création objet
                self.ecrire_param ( 'liste_1', self.tri('liste_1').formate_liste() )
                self.effacer_params('tri')

            elif c=='R':   # dimension des listes
                self.dimension = self.saisir_entier("Dimension des listes: ")
                if self.dimension:
                    self.ecrire_param('dimension', self.dimension)
                    self.effacer_params('tri')
                    # raz liste_2
                    self.ecrire_param ('liste_2', '')
                    self.installer_tri('liste_2', None )

            elif c=='G':    # Générer liste aléatoire
                if self.dimension:
                    self.installer_tri( 'liste_2', Tri(Tri.liste_aleatoire(self.dimension)) )  # création objet
                    self.ecrire_param ( 'liste_2', self.tri('liste_2').formate_liste())
                    self.effacer_params('tri')

            elif c=='C':   # Quelle liste ?
                self.ecrire_param('liste', self.change_liste() )
                self.effacer_params('tri')

            elif c=='A':   # Afficher la liste courante
                tri = self.tri()
                if tri:
                    self.afficher_message(tri.liste())

            elif c=='1':    # Tri à bulle
                tri = self.tri()
                if tri:
                    L = tri.bulle()
                    self.afficher_message( L )
                    self.ecrire_param('tri_bulle', Tri.resultat(L, tri.compteurs()) )

            elif c=='2':    # Tri sélection
                tri = self.tri()
                if tri:
                    L = tri.selection()
                    self.afficher_message( L )
                    self.ecrire_param('tri_selection', Tri.resultat(L, tri.compteurs()) )

            elif c=='3':    # Tri insertion
                tri = self.tri()
                if tri:
                    L = tri.insertion()
                    self.afficher_message( L )
                    self.ecrire_param('tri_insertion', Tri.resultat(L, tri.compteurs()) )

            elif c=='4':    # Tri fusion
                tri = self.tri()
                if tri:
                    L = tri.fusion()
                    self.afficher_message( L )
                    self.ecrire_param('tri_fusion', Tri.resultat(L, tri.compteurs()) )

            elif c=='5':    # Tri rapide
                tri = self.tri()
                if tri:
                    L = tri.rapide()
                    self.afficher_message( L )
                    self.ecrire_param('tri_rapide', Tri.resultat(tri.rapide(), tri.compteurs()) )
# fin
