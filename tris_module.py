# encoding: utf-8
#
# tris_module
#   modules de tri:
#       tri à bulle
#       tri par selection
#       tri par insertion
#       tri par fusion
#       tri rapide
# @author: denis <denis.defolie@e-educ.fr>
#                <denis.defolie@free.fr>
#

from datetime import datetime
from random import randrange

class Tri(object):
    '''
    Module de tri
    '''
    def __init__(self, L):
        '''
        Constructeur
        @param L: liste d'entiers
        '''
        self.L = L

    def liste(self):
        '''
        Retourne la liste créée par __init__()
        @return: list liste L
        '''
        return self.L

    def tri_bulle(self, L):
        """
        Tri à bulle
        Comparer, en commençant par la fin de la liste, les couples d’éléments successifs.
        Lorsque deux éléments successifs ne sont pas dans l’ordre croissant, ils sont échangés.
        On fait ainsi remonter le terme le plus petit.
        Puis on itère le procédé sur le sous-liste restant

        @param L: liste à trier
        @return: L
        -----------------------------
        Solution avec range ou xrange:
        -----------------------------
        n = len(L)
        for i in range(n-1):
            for j in range(n-1, i, -1):
                if L[j] < L[j-1]:
                    L[j], L[j-1] = L[j-1], L[j]     #permutation
                    counts+=1
                loops+=1
        return L
        """
        i = 0                       # i = index de parcours du liste
        n = len(L)-1                # n = nb d'éléments
        while i < n:                # parcours de 0  n-1 éléments soit n boucles
            j = n                   # j < n dernier élément
            while i < j:            # parcours à l'envers de n-1 à j
                if L[j] < L[j-1]:   # remonter le + petit
                    L[j], L[j-1] = L[j-1], L[j]     # permutation
                    self.tests+=1   # comptage tests
                j-=1                # pas = -1
                self.boucles+=1     # comptage boucles
            i+=1                    # index++
            self.boucles+=1
        return L

    def tri_selection(self, L):
        """
        Tri par sélection
        On détermine la position du plus petit élément, on le met en première position
        en échangeant avec le premier élément) jusqu'à la fin du liste restant.
        @param L: list liste L
        @return: list L triée
        """
        n = len(L)
        i = 0
        while i < n:
        #for i in range(n):
            m = i                   # init m mini
            j = i+1
            while j < n:
            #for j in range(i+1, n): # trouver le + petit
                if L[j] < L[m]:
                    m = j           # mini courant
                    self.tests+=1
                j+=1
                self.boucles+=1
            L[i], L[m] = L[m], L[i] # mini au début, permuter i courant en m
            i+=1
            self.boucles+=1
        return L

    def tri_insertion(self, L):
        i=1
        while i < len(L):
        #for i in range(1, len(L)):
            j = i - 1
            tmp = L[i]
            while j > -1 and L[j] > tmp:
                L[j+1] = L[j]
                j -= 1
                self.tests+=2
                self.boucles+=1
            L[j+1] = tmp
            i+=1
            self.boucles+=1
        return L

    def tri_fusion(self, L):
        """
        Tri rapide par fusion
        La fonction de tri, récursive, consiste à couper en deux la liste initiale,
        et trier (par appels récursifs) les deux sous-listes, puis de les fusionner
        en faisant appel à la fonction fusion.
        @param L: list L à trier
        @return list L triée
        """
        def fusion(L, M):
            """
            Fusionne 2 listes
            @param L: liste ordonnée
            @param M: liste ordonnée
            @return: C: liste fusionnée et ordonnée
            """
            C = []
            i, j, l, m = 0, 0, len(L), len(M)   # indices des taleaux L, M et tailles L, M
            while i < l and j < m:

                if L[i] < M[j]:
                    C.append(L[i])
                    i+=1
                else:
                    C.append(M[j])
                    j+=1
                self.tests+=1
                self.boucles+=1
            if i == l:                  # L terminé donc poursuivre M
                while j < m:
                    self.boucles+=1
                    C.append(M[j])
                    j+=1
            else:                       # Sinon poursuivre L
                while i < l:
                    self.boucles+=1
                    C.append(L[i])
                    i+=1
            return C

        n = len(L)      # Taille liste
        if n <= 1:      # fin récursivité si plus rien à découper
            return L    #
        m = n//2        # Indice milieu
        return fusion( self.tri_fusion(L[0:m]), self.tri_fusion(L[m:]) )

    def tri_rapide(self, L):
        """
        tri rapide (quicksort) de la liste L
        @param L: list L à trier
        @return list L triée
        """
        def trirapide(L, g, d):
            pivot = L[(g+d)//2]
            i = g
            j = d
            while True:
                self.boucles+=1
                while L[i]<pivot:
                    i+=1
                    self.tests+=1
                    self.boucles+=1
                while L[j]>pivot:
                    j-=1
                    self.tests+=1
                    self.boucles+=1
                if i>j:
                    self.tests+=1
                    break
                if i<j:
                    self.tests+=1
                    L[i], L[j] = L[j], L[i]
                i+=1
                j-=1
            if g<j:
                self.tests+=1
                trirapide(L, g, j)
            if i<d:
                self.tests+=1
                trirapide(L, i, d)
        g=0
        d=len(L)-1
        trirapide(L, g, d)
        return L

    def debut(self):
        self.T0, self.duree, self.tests, self.boucles = datetime.now(), 0, 0, 0

    def fin(self):
        t = datetime.now() - self.T0
        self.duree = t.total_seconds()

    def compteurs(self):
        '''
        Retourn un tuple des compteurs
        @return: tuple des compteurs
        '''
        return  (self.tests, self.boucles, self.duree)

    def formate_liste(self):
        return '{} ( x{} )'.format(Tri.liste_reduite(self.L), len(self.L) )

    @staticmethod
    def formate_compteurs(compteurs):
        '''
        Retourne le tuple compteurs en chaîne formatée.
            ps: *compteurs pour décompacter le tupple (a, b, ...)
        @param compteurs: tuple compteurs
        @return: string compteurs formaté
        '''
        if not compteurs:
            return ''
        return '[Comp.: {:10}] - [Boucles: {:10}] - [Durée: {:9.6f} s]'.format(*compteurs)

    @staticmethod
    def liste_reduite(L):
        '''
        Decoupe une liste pour affichage simplifié
        @param L: list list L
        @return: string liste découpée
        '''
        s = str(L)
        if len(L)>16:
            s = '[{}, ..., {}]'.format ( str(L[:8]).strip('[]'), str(L[-8:]).strip('[]') )
        return s

    @staticmethod
    def resultat(L, compteurs=''):
        '''
        Retourne le résultat formaté
        @param L: liste
        @param compteurs:  tuple compteurs
        @return: string résultat formaté
        '''
        return '{} ( x{} )  {}'.format(Tri.liste_reduite(L), len(L), Tri.formate_compteurs(compteurs) )

    @staticmethod
    def liste_aleatoire(dimension=50):
        '''
        liste aléatoire d'integer de dimension dimension >0
        @param dimension: int dimension liste
        '''
        dimension = abs(dimension)
        return [randrange(0, dimension//2) for i in range(dimension)] #@UnusedVariable

    def _tri(self, fonction_tri):
        '''
        Méthode privée.
        @param fonction_tri: fonction de tri
        @return list copie de liste
        '''
        self.debut()
        result = fonction_tri( list(self.L) )
        self.fin()
        return result

    def bulle(self):
        return self._tri(self.tri_bulle)

    def selection(self):
        return self._tri(self.tri_selection)

    def fusion(self):
        return self._tri(self.tri_fusion)

    def insertion(self):
        return self._tri(self.tri_insertion)

    def rapide(self):
        return self._tri(self.tri_rapide)

    def rapide_indexe(self):
        return self._tri(self.tri_rapide_indexe)

    def __str__(self):
        return str(L)
#
# Usage
#
if __name__ == '__main__':
    L = Tri.liste_aleatoire(500)
    tri = Tri(L)
    print("tri bulle    :", Tri.resultat( tri.bulle(),     tri.compteurs()))
    print("tri selection:", Tri.resultat( tri.selection(), tri.compteurs()))
    print("tri insertion:", Tri.resultat( tri.insertion(), tri.compteurs()))
    print("tri fusion   :", Tri.resultat( tri.fusion(),    tri.compteurs()))
    print("tri rapide   :", Tri.resultat( tri.rapide(),    tri.compteurs()))

    print()
