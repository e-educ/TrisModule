# Tris Module
***

## Description
* Module de tri en python2 et python3 (tris_module.py)
* La démo est en python3

## Fonctions (APIs)
* tri à bulle
* tri par selection
* tri par insertion
* tri par fusion
* tri rapide

## Usages
* ***Tester le module:*** python3 tris_module.py
* ***Démo:*** python3 tris.py
* ***La doc est intégrée aux fichiers***


(c) denis@e-educ.fr
