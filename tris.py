#! /usr/bin/env python3
# encoding: utf-8
#
#  tris
'''
TRIS DE TABLEAUX PAR COMPARAISON
    On utilise 2 classes:
        Tri gestionnaire des tris.
            Les instances sont créées au fur et à mesure des besoins
            methodes de tri
                bulle, selection, insertion, fusion, rapide, ...
            methodes statics:
                formatages de données principalement

        Ui  l'interface console pour linux, mac, ou windows.
            Une seule instance dans le programme principal
            methode principale
                loop

    main    Programme principal
            Les paramètres de l'application sont stockés dans un dictionnaire de données 'params'

    usage: python3 tris.py
'''
from tris_interface import Ui

gabarit = """
TRIS DE TABLEAUX PAR COMPARAISON

    (S)aisir une liste d'entiers       liste_1: {liste_1}
    (G)énérer une liste aléatoire.     liste_2: {liste_2}
    (C)hanger la liste d'essai: {liste}
    ------------------------------------------------------

    (1) Tri à bulle.   {liste} {tri_bulle}
    (2) Tri sélection. {liste} {tri_selection}
    (3) Tri insertion. {liste} {tri_insertion}
    (4) Tri fusion.    {liste} {tri_fusion}
    (5) Tri rapide.    {liste} {tri_rapide}

    --[ options ]-----------------------------------------

    (A)fficher le détail de la liste d'essai {liste}
    (R)az et saisir la dimension des listes ( x{dimension} )

    ------------------------------------------------------
    (Q)uitter

------"""

detail_msg = """
----------------------------
Détail de la liste {}
----------------------------
{}
"""

'''
Paramètres de l'application
Données dans un dictionnaire sous forme de clef: valeur
'''
params = {
    'gabarit'       : gabarit,      # gabarit écran
    'dimension'     : 50,           # dimension des listes
    'detail_msg'    : detail_msg,   # entête détail liste
    'liste'         : '',           # nom liste courante
    'liste_1'       : '',
    'liste_2'       : '',
    'tri_bulle'     : '',
    'tri_selection' : '',
    'tri_insertion' : '',
    'tri_fusion'    : '',
    'tri_rapide'    : '',
    'tri_fusion'    : '',
}

if __name__ == '__main__':
    ui  = Ui(**params)  # Objet Interface. On utilise le décompactage des dictionnaires **
    ui.loop()           # Le programme commence ici
    exit(0)

# fin
